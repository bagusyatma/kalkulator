let input1 = document.getElementById("input1");
let input2 = document.getElementById("input2");

let txtHasil = document.getElementById("hasil");

function tambah() {
  let hasil = parseFloat(input1.value) + parseFloat(input2.value);
  txtHasil.innerHTML = hasil;
}

function kurang() {
  let hasil = parseFloat(input1.value) - parseFloat(input2.value);
  txtHasil.innerHTML = hasil;
}

function kali() {
  let hasil = parseFloat(input1.value) * parseFloat(input2.value);
  txtHasil.innerHTML = hasil;
}

function bagi() {
  let hasil = parseFloat(input1.value) / parseFloat(input2.value);
  txtHasil.innerHTML = hasil;
}

function modulus() {
  let hasil = parseFloat(input1.value) % parseFloat(input2.value);
  txtHasil.innerHTML = hasil;
}

function pangkat() {
  let hasil = parseFloat(input1.value) ** parseFloat(input2.value);
  txtHasil.innerHTML = hasil;
}

function akar2() {
  const formInput = document.getElementById("form");
  let inputAkar = document.getElementById("inputAkar");

  formInput.innerHTML = `<input type="number" id="inputAkar" placeholder="Masukan angka"> <br><br>`;
  if (inputAkar.value != "") {
    txtHasil.innerHTML += Math.sqrt(parseInt(inputAkar.value));
  }
}

function akar3() {
  const formInput = document.getElementById("form");
  let inputAkar = document.getElementById("inputAkar");

  formInput.innerHTML = `<input type="number" id="inputAkar" placeholder="Masukan angka"> <br><br>`;
  if (inputAkar.value != "") {
    txtHasil.innerHTML += Math.cbrt(parseInt(inputAkar.value));
  }
}

function reset() {
  input1.value = "";
  input2.value = "";

  location.reload();
}
